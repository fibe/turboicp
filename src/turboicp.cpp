///  TurboICP
///  --------------------------------------------------------------------------------
///
///  DigitalMetrix S.r.l.  CONFIDENTIAL
//   Unpublished Copyright (c) 2020-2021 DigitalMetrix S.r.l., All Rights Reserved.
///
///  NOTICE:  All information contained herein is, and remains the property of
///  DigitalMetrix S.r.l.  The intellectual and technical concepts contained herein
///  are proprietary to DigitalMetrix S.r.l. and may be covered by Italian and
///  Foreign Patents, patents in process, and are protected by trade secret or
///  copyright law.  Dissemination of this information or reproduction of this
///  material is strictly forbidden unless prior written permission is obtained from
///  DigitalMetrix S.r.l.  Access to the source code contained herein is hereby
///  forbidden to anyone except current DigitalMetrix S.r.l. employees, managers or
///  contractors who have executed Confidentiality and Non-disclosure agreements
///  explicitly covering such access.
///
///  The copyright notice above does not evidence any actual or intended publication
///  or disclosure  of  this source code, which includes information that is
///  confidential and/or proprietary, and is a trade secret, of  DigitalMetrix
///  S.r.l.  ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE, OR
///  PUBLIC DISPLAY OF OR THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS
///  WRITTEN CONSENT OF DigitalMetrix S.r.l. IS STRICTLY PROHIBITED, AND IN
///  VIOLATION OF APPLICABLE LAWS AND INTERNATIONAL TREATIES.  THE RECEIPT OR
///  POSSESSION OF  THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR
///  IMPLY ANY RIGHTS TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO
///  MANUFACTURE, USE, OR SELL ANYTHING THAT IT  MAY DESCRIBE, IN WHOLE OR IN PART.
///

#include <iostream>
#include <fstream>
#include <CGAL/Simple_cartesian.h>
#include <CGAL/AABB_tree.h>
#include <CGAL/AABB_traits.h>
#include <CGAL/Surface_mesh.h>
#include <CGAL/AABB_face_graph_triangle_primitive.h>
#include <CGAL/Polygon_mesh_processing/compute_normal.h>
#include <CGAL/Polygon_mesh_processing/orientation.h>

#include "Eigen/Dense"
#include "Eigen/Geometry"

typedef CGAL::Simple_cartesian<double> K;
typedef K::FT FT;
typedef K::Point_3 Point;
typedef K::Vector_3 Vector;
typedef std::pair<Point, Vector> OrientedPoint;
typedef K::Ray_3 Ray;
typedef CGAL::Surface_mesh<Point> Mesh;
typedef boost::graph_traits<Mesh>::face_descriptor face_descriptor;
typedef boost::graph_traits<Mesh>::halfedge_descriptor halfedge_descriptor;
typedef CGAL::AABB_face_graph_triangle_primitive<Mesh> Primitive;
typedef CGAL::AABB_traits<K, Primitive> Traits;
typedef CGAL::AABB_tree<Traits> Tree;
typedef boost::optional<Tree::Intersection_and_primitive_id<Ray>::Type> Ray_intersection;


// Utility functions
template <typename T>
Eigen::Vector3d CGAL2Eigen(const T& p) { return Eigen::Vector3d(p.x(), p.y(), p.z()); }


template <typename T>
T Eigen2CGAL(const Eigen::Vector3d& p) { return T(p[0], p[1], p[2]); }


template <typename T>
T parse_from_str(std::string s)
{
    T val;
    std::stringstream ss(s);
    ss >> val;
    return val;
}



/// <summary>
/// Computes the RMS between corresponding point sets
/// </summary>
/// <param name="ptsA">First point set</param>
/// <param name="ptsB">Second point set</param>
/// <param name="R">Rotation matrix applied to ptsB</param>
/// <param name="T">Translation vector applied to ptsB</param>
/// <returns>Root Mean Square error, ie.   rms( ptsA - (R*ptsB + T) )</returns>
double compute_rms(std::vector< Eigen::Vector3d > ptsA, std::vector< Eigen::Vector3d > ptsB, Eigen::Matrix3d R, Eigen::Vector3d T)
{
    double rms = 0;

    for (size_t ii = 0; ii < ptsA.size(); ++ii)
    {
        rms += (ptsA[ii] - (R * ptsB[ii] + T)).squaredNorm();
    }
    rms /= static_cast<double>(ptsA.size());
    return std::sqrt(rms);
}


/// <summary>
/// Computes the rotation R and translation T minimising the rms error between (R*ptsB+T) and ptsA
/// </summary>
/// <param name="ptsA">First point set</param>
/// <param name="ptsB">Second point set</param>
/// <returns>The roto-translation (R,T)</returns>
std::pair< Eigen::Matrix3d, Eigen::Vector3d > absor(std::vector< Eigen::Vector3d > ptsA, std::vector< Eigen::Vector3d > ptsB)
{
    // Compute centroids
    Eigen::Vector3d cA(0,0,0);
    Eigen::Vector3d cB(0,0,0);

    for (auto v : ptsA)
    {
        cA += v;
    }
    cA /= static_cast<double>(ptsA.size());

    for (auto v : ptsB)
    {
        cB += v;
    }
    cB /= static_cast<double>(ptsB.size());


    // Compute covariance matrix H
    Eigen::Matrix3d H = Eigen::Matrix3d::Zero();
    for (size_t ii = 0; ii < ptsA.size(); ++ii)
    {
        Eigen::Vector3d va = ptsA[ii] - cA;
        Eigen::Vector3d vb = ptsB[ii] - cB;

        H += vb * va.transpose();
    }

    // Compute SVD of H
    Eigen::JacobiSVD< Eigen::MatrixXd > svd(H, Eigen::ComputeFullU | Eigen::ComputeFullV /*, Eigen::ComputeThinU | Eigen::ComputeThinV */ );
    const auto& V = svd.matrixV();
    const auto& U = svd.matrixU();

    Eigen::Matrix3d D = Eigen::Matrix3d::Identity();
    D(2, 2) = (V * U.transpose()).determinant();

    // Compute R and T
    const auto R = V * D * U.transpose();
    const auto T = -R*cB+cA;

    return std::make_pair(R, T);
}


#if 0
void absor_test()
{
    srand(time(0));
    std::vector< Eigen::Vector3d > ptsA;
    std::vector< Eigen::Vector3d > ptsB;

    //Eigen::Matrix3d R = Eigen::Matrix3d::Identity();
	Eigen::Vector3d rotAxis = Eigen::Vector3d(static_cast<double>(rand()) / RAND_MAX - 0.5, static_cast<double>(rand()) / RAND_MAX - 0.5, static_cast<double>(rand()) / RAND_MAX - 0.5).normalized();
    double rotAngle = static_cast<double>(rand()) / RAND_MAX * 6.28;

    Eigen::Matrix3d R = Eigen::AngleAxisd(rotAngle, rotAxis).toRotationMatrix();
    Eigen::Vector3d T(10, 10, -2);

    std::cout << R << std::endl;

    auto Rinv = R.transpose();
    auto Tinv = -Rinv * T;

    for (size_t ii = 0; ii < 5; ++ii)
    {
        const Eigen::Vector3d p(static_cast<double>(rand()) / RAND_MAX, static_cast<double>(rand()) / RAND_MAX, static_cast<double>(rand()) / RAND_MAX);
		ptsA.push_back( p );
		ptsB.push_back( Rinv*p + Tinv );
    }

    std::cout << "Initial RMS: " << compute_rms(ptsA, ptsB, Eigen::Matrix3d::Identity(),  Eigen::Vector3d(0,0,0) ) << std::endl;
    std::cout << "Initial RMS RT: " << compute_rms(ptsA, ptsB, R, T) << std::endl;


    auto pose = absor(ptsA, ptsB );
    std::cout << "Absor RMS: " << compute_rms(ptsA, ptsB, pose.first, pose.second) << std::endl;

}
#endif


/// <summary>
/// Helper function to load a ply file to a CGAL Surface Mesh
/// </summary>
/// <param name="filename"></param>
/// <param name="m">output mesh</param>
/// <returns>True on success, false otherwise.</returns>
bool load_ply_file( std::string filename, Mesh& m )
{
	std::ifstream ifs(filename, std::ios::binary);

	if (!ifs.is_open())
	{
		std::cerr << "Unable to open " << filename << std::endl;
		return false;
	}

	std::cout << "Loading " << filename << " ..... ";
	if ( CGAL::read_ply(ifs, m ))
	{
		std::cout << " OK: " << m.num_vertices() << " vertices, " << m.number_of_faces() << " faces." << std::endl;
	}
	else
	{
		std::cout << "ERROR." << std::endl;
		return false;
	}

    return true;
}


int main(int argc, char* argv[])
{

    std::cout << " DigitalMetrix' TurboICP v. 1.1" << std::endl;
    std::cout << " Copyright (c) 2020-2021 DigitalMetrix S.r.l., All Rights Reserved." << std::endl;
    std::cout << "===================================================================" << std::endl;

    if( argc!=9 )
    {
        std::cout << "Usage: " << std::endl;
        std::cout << "turboicp  <model.ply> <data.ply> MAX_DIST N_POINTS N_ITERS TRIM_LOW% TRIM_HIGH% <out.txt>" << std::endl;
        std::cout << std::endl << std::endl;
        std::cout << "TurboICP computes the best rigid motion (R,T) to register model mesh to data." << std::endl;
        std::cout << "ie. Apply (R,T) to model.ply to align it against data.ply" << std::endl;
        std::cout << std::endl << std::endl;
        std::cout << "Model must be a triangulated mesh, data must contain points and normals. ICP is" << std::endl;
        std::cout << "performed via normal shooting (both ways, in the normal direction and the opposite)" << std::endl;
        std::cout << "and matches are kept if their relative distance is below MAX_DIST." << std::endl;
        std::cout << "Match distances are then sorted and trimmed between TRIM_LOW% ... TRIM_HIGH% (good values are 40 ... 90)" << std::endl;
        std::cout << "Output file will contain a 3x4 (row-wise) [R T] matrix on success. Errors and stats" << std::endl;
        std::cout << "are reported on the console. Good luck!" << std::endl << std::endl;
        return 0;
    }


    Mesh model;
    Mesh data;
    const double max_match_dist = parse_from_str<double>(argv[3]);
    const size_t n_points = parse_from_str<size_t>(argv[4]);
    const size_t n_iters = parse_from_str<size_t>(argv[5]);
    const size_t trim_low = parse_from_str<size_t>(argv[6]);
    const size_t trim_high = parse_from_str<size_t>(argv[7]);

    if (!load_ply_file( argv[1], model))
    {
        return -1;
    }

    if (!load_ply_file( argv[2], data))
    {
        return -1;
    }


    std::cout << "Creating model AABBTree...";
    Tree tree(faces(model).first, faces(model).second, model);
    std::cout << "OK" << std::endl;


    auto normal_map = data.property_map< Mesh::vertex_index, Vector >("v:normal").first;


    Eigen::Matrix3d R = Eigen::Matrix3d::Identity();
    Eigen::Vector3d T(0, 0, 0);

	std::vector< bool > points_to_use(data.number_of_vertices(), false);
	std::fill(points_to_use.begin(), points_to_use.begin() + std::min( n_points, points_to_use.size()-1 ), true);

    std::cout << "------------------- starting ICP (" << n_iters << " iters, " << max_match_dist << " max dist, " << trim_low << ".." << trim_high << " trimming)" << std::endl;

    for (size_t icp_it = 0; icp_it < n_iters; ++icp_it)
    {
        std::cout << "Iteration " << std::setw(4) << icp_it+1;

        std::vector< Eigen::Vector3d > model_matches;
        std::vector< Eigen::Vector3d > data_matches;
        std::vector< double > distances;

        std::random_shuffle(std::begin(points_to_use), std::end(points_to_use));
        
        size_t kk=0;
        for (auto vit = data.vertices_begin(); vit != data.vertices_end(); ++vit)
        {
            if (!points_to_use[kk++])
                continue;

            Point  p = Eigen2CGAL<Point>(R * CGAL2Eigen(data.point(*vit)) + T);
            Vector n = Eigen2CGAL<Vector>(R * CGAL2Eigen(normal_map[*vit]));

            // Two rays, one directed as normal and the other in opposite direction
            Ray ray1(p, -n);
            Ray ray2(p, n);

            Ray_intersection intersection1 = tree.first_intersection(ray1);
            Ray_intersection intersection2 = tree.first_intersection(ray2);

            double dist1=1E99;
            Point p1;
            double dist2=1E99;
            Point p2;

            bool found = false;

			if ( intersection1 && boost::get<Point>(&(intersection1->first)))
			{
				p1 = *boost::get<Point>(&(intersection1->first));
				dist1 = std::sqrt((p1 - p).squared_length());
                found = true;
			}
			if ( intersection2 && boost::get<Point>(&(intersection2->first)))
			{
				p2 = *boost::get<Point>(&(intersection2->first));
				dist2 = std::sqrt((p2 - p).squared_length());
                found = true;
			}

            if ( found )
            {
                double mindist = dist1 < dist2 ? dist1 : dist2;
                if (mindist < max_match_dist)
                {
                    Point ptmodel = dist1 < dist2 ? p1 : p2;
                    // A good match was found
                    model_matches.push_back(Eigen::Vector3d(ptmodel.x(), ptmodel.y(), ptmodel.z()));
                    data_matches.push_back(Eigen::Vector3d(p.x(), p.y(), p.z()));
                    distances.push_back( mindist );
                }
            }

        }

        std::cout << ", # matches: " << std::setw(8) << model_matches.size();

#if 0
        {
            //Debug intersections
            std::ofstream fout("intersections.xyz");

            for (auto pt : model_matches)
            {
                fout << pt[0] << " " << pt[1] << " " << pt[2] << std::endl;
            }
            fout.close();
        }
#endif

        if (model_matches.size() < 5)
        {
            std::cout << std::endl << "Too few matches, aborting." << std::endl;
            break;
        }


        // Sort data indices by distances in descending order
        std::vector<std::size_t> index(distances.size());
        std::iota(index.begin(), index.end(), 0); // 0,1,2,...

        std::sort(index.begin(), index.end(), [&]( const size_t& a, const size_t& b) {
            return distances[a] < distances[b];
        });

        /*
        for (size_t ii = 0; ii < 5; ++ii)
        {
            std::cout << distances[ index[ii] ] << "  " << std::endl;
        }
        */


        // Take the n% farthest points
        std::vector< Eigen::Vector3d > model_matches_trimmed;
        std::vector< Eigen::Vector3d > data_matches_trimmed;
        
        size_t npts_close = distances.size() * trim_low / 100;
        size_t npts_far = distances.size() * trim_high / 100;

        /*
        for (size_t ii = 0; ii < npts_close; ++ii) 
        {
            model_matches_trimmed.push_back(model_matches[index[ii]]);
            data_matches_trimmed.push_back(data_matches[index[ii]]);
        }
        for (size_t ii = npts_farth; ii < distances.size(); ++ii) 
        {
            model_matches_trimmed.push_back(model_matches[index[ii]]);
            data_matches_trimmed.push_back(data_matches[index[ii]]);
        }
        */

        for (size_t ii = npts_close; ii < npts_far; ++ii) 
        {
            model_matches_trimmed.push_back(model_matches[index[ii]]);
            data_matches_trimmed.push_back(data_matches[index[ii]]);
        }
        std::cout << ", " << model_matches_trimmed.size() << " after trimming";


        std::cout << ", initial RMS: " << std::setw(15) << compute_rms(model_matches_trimmed, data_matches_trimmed, Eigen::Matrix3d::Identity(), Eigen::Vector3d(0,0,0) );
        auto pose = absor(model_matches_trimmed, data_matches_trimmed);
        std::cout << ", Absor RMS: " << compute_rms(model_matches_trimmed, data_matches_trimmed, pose.first, pose.second ) << std::endl;

        auto newR = pose.first * R;
        auto newT = pose.first * T + pose.second;
        R = newR;
        T = newT;

    }

    std::cout << "------------------- Done!" << std::endl;

    {
        Mesh newdata( data );
        for (auto vit = newdata.vertices_begin(); vit != newdata.vertices_end(); ++vit)
        {
            Point  p = Eigen2CGAL<Point>(R * CGAL2Eigen(newdata.point(*vit)) + T);
            newdata.point(*vit) = p;
        }
        
        std::ofstream fout("aligned.ply", std::ios::binary);
        CGAL::write_ply(fout, newdata);
        fout.close();
    }

    {
        auto Rinv = R.transpose();
        auto Tinv = -Rinv * T;

        std::ofstream ofs( argv[8] );
        ofs << std::setprecision(10);
        for(int ii=0;ii<3;++ii )
			ofs << Rinv(ii, 0) << " " << Rinv(ii, 1) << " " << Rinv(ii, 2) << " " << Tinv(ii) << std::endl;
        ofs.close();
    }

    return 0;

}
