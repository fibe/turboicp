# TurboICP

A simple yet effective ICP implementation (oriented-points vs. triangulated mesh) accelerated by CGAL AABBTree

---

```
DigitalMetrix S.r.l.  CONFIDENTIAL
Unpublished Copyright (c) 2020-2021 DigitalMetrix S.r.l., All Rights Reserved.
```

